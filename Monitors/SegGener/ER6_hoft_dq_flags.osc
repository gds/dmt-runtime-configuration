#
#   ER3_hoft_DQ_flags.osc  contains the flags in the LLD data quality vector
#
#   Change list:
#   Date        Author      Change
#   2012-12-02  J. Zweizig  Remove H2 flags.
#   2014-11-28  J. Zweizig  New flags from gstlal_compute_strain
#
# H1:hoft_Science    bitand "H1:LLD-DQ_VECTOR" mask=0x01 fraction=1.0
# H1:hoft_Up         bitand "H1:LLD-DQ_VECTOR" mask=0x02 fraction=1.0
# H1:hoft_Calibrated bitand "H1:LLD-DQ_VECTOR" mask=0x04 fraction=1.0
# H1:hoft_Ready      boolean "H1:hoft_Science & H1:hoft_Calibrated"
#
# L1:hoft_Science    bitand "L1:LLD-DQ_VECTOR" mask=0x01 fraction=1.0
# L1:hoft_Up         bitand "L1:LLD-DQ_VECTOR" mask=0x02 fraction=1.0
# L1:hoft_Calibrated bitand "L1:LLD-DQ_VECTOR" mask=0x04 fraction=1.0
# L1:hoft_Ready      boolean "L1:hoft_Science & L1:hoft_Calibrated"
#
#  Bits from ER6 gstlal_compute_strain
#
#  bit   Meaning
#   0    HOFT_OK
#   1    SCIENCE_LOCKED
#   2    LOCKED
#   3    HOFT_PROD
#   4    FILTERS_OK
#   5    GAMMA_OK
#
H1:hoft_Science    bitand "H1:GDS-CALIB_STATE_VECTOR" mask=0x02 fraction=1.0
H1:hoft_Up         bitand "H1:GDS-CALIB_STATE_VECTOR" mask=0x04 fraction=1.0
H1:hoft_Calibrated bitand "H1:GDS-CALIB_STATE_VECTOR" mask=0x01 fraction=1.0
H1:hoft_Ready      bitand "H1:GDS-CALIB_STATE_VECTOR" mask=0x03 fraction=1.0
#
L1:hoft_Science    bitand "L1:GDS-CALIB_STATE_VECTOR" mask=0x02 fraction=1.0
L1:hoft_Up         bitand "L1:GDS-CALIB_STATE_VECTOR" mask=0x04 fraction=1.0
L1:hoft_Calibrated bitand "L1:GDS-CALIB_STATE_VECTOR" mask=0x01 fraction=1.0
L1:hoft_Ready      bitand "L1:GDS-CALIB_STATE_VECTOR" mask=0x03 fraction=1.0
#
V1:hoft_data_OK valueabove "V1:Hrec_Veto_dataQuality" threshold=7.9 fraction=1
V1:hoft_Science valueabove "V1:Hrec_Veto_dataQuality" threshold=11.9 fraction=1
#
# Check for invalid data from calibration pipeline
L1:DMT_ODC_MASTER_CALIB_GT_ZERO_s valueabove "L1:ODC-MASTER_CHANNEL_OUT_DQ" threshold=0 fraction=0
L1:DMT_ODC_MASTER_CALIB_LT_ZERO_s valuebelow "L1:ODC-MASTER_CHANNEL_OUT_DQ" threshold=0 fraction=0
L1:DMT_INVALID_CALIBRATED_DATA_s boolean "!(L1:DMT_ODC_MASTER_CALIB_GT_ZERO_s | L1:DMT_ODC_MASTER_CALIB_LT_ZERO_s)"
L1:DMT_INVALID_CALIBRATED_DATA_TST_s valuerange "L1:ODC-MASTER_CHANNEL_OUT_DQ" fraction=0 lo=-0.5 hi=0.5
# Attempting to generate single flag for zero ODC channel copy using two different bitmask representations
L1:DMT_INVALID_CALIBRATED_DATA_TWO_s bitnor "L1:ODC-MASTER_CHANNEL_OUT_DQ" fraction=0 mask=-1
L1:DMT_INVALID_CALIBRATED_DATA_THREE_s bitnor "L1:ODC-MASTER_CHANNEL_OUT_DQ" fraction=0 mask=0xffffffff
# Flag for picking out when Calibration filters are bad: (bitnand fraction=0 means any sample for which this bit is zero activates the segment for a second)
L1:DMT-CALIB_FILTER_NOT_OK_s bitnand "L1:GDS-CALIB_STATE_VECTOR" mask=0x10 fraction=0
# H1 checks
H1:DMT_ODC_MASTER_CALIB_GT_ZERO_s valueabove "H1:ODC-MASTER_CHANNEL_OUT_DQ" threshold=0 fraction=0
H1:DMT_ODC_MASTER_CALIB_LT_ZERO_s valuebelow "H1:ODC-MASTER_CHANNEL_OUT_DQ" threshold=0 fraction=0
H1:DMT_INVALID_CALIBRATED_DATA_s boolean "!(H1:DMT_ODC_MASTER_CALIB_GT_ZERO_s | H1:DMT_ODC_MASTER_CALIB_LT_ZERO_s)"
# Attempting to generate single flag for zero ODC channel copy using two different bitmask representations
H1:DMT_INVALID_CALIBRATED_DATA_THREE_s bitnor "H1:ODC-MASTER_CHANNEL_OUT_DQ" fraction=0 mask=0xffffffff
# Flag for picking out when Calibration filters are bad: (bitnand fraction=0 means any sample for which this bit is zero activates the segment for a second)
H1:DMT-CALIB_FILTER_NOT_OK_s bitnand "H1:GDS-CALIB_STATE_VECTOR" mask=0x10 fraction=0
# H1:DMT-DQ_VECTOR Channel Segments
H1:DMT_DQ_VECTOR_BIT0_VETO_s bitnand "H1:DMT-DQ_VECTOR" mask=0x01 fraction=0
H1:DMT_DQ_VECTOR_BIT1_VETO_s bitnand "H1:DMT-DQ_VECTOR" mask=0x02 fraction=0
H1:DMT_DQ_VECTOR_BIT2_VETO_s bitnand "H1:DMT-DQ_VECTOR" mask=0x04 fraction=0
# L1:DMT-DQ_VECTOR Channel Segments
L1:DMT_DQ_VECTOR_BIT0_VETO_s bitnand "L1:DMT-DQ_VECTOR" mask=0x01 fraction=0
L1:DMT_DQ_VECTOR_BIT1_VETO_s bitnand "L1:DMT-DQ_VECTOR" mask=0x02 fraction=0
L1:DMT_DQ_VECTOR_BIT2_VETO_s bitnand "L1:DMT-DQ_VECTOR" mask=0x04 fraction=0

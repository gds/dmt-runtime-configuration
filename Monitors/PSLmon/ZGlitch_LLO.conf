#
#   Specialized Glitch Monitor
#   __________________________
#
#   This monitor looks at known glitch sources and generates online segments
#   when those glitches are seen.
#
#   Date/Author  Changes
#   Jan 26, 2010 Cut back ultra-sleek version.
#		   * Remove all unfiltered PEMs, IFO control signale
#		   * Add L1:SUS-ETMY_SENSOR_SIDE segments
#   May 4, 2009  S6 Version: 
#                  * Replace AS_Q with DARM_ERR.
#                  * Remove trigger enable
#                  * Add segment generation.
#
Parameter MonName ZGlitch
Parameter TrigEnable 1
Parameter Stride 4.0
Parameter OSCFile "LockLoss.conf"

Channel L0:PEM-EX_V1
Channel L0:PEM-EY_V1
Channel L1:LSC-DARM_CTRL_EXC_DAQ
Channel L1:SUS-ETMY_SENSOR_SIDE

#TCS Channels
Channel L1:TCS-ITMX_PD_ISS_OUT_AC
Channel L1:TCS-ITMY_PD_ISS_OUT_AC

#
#---> Filters
Filter HP2048  Design -settle 0.25 2048 "ellip('HighPass',4,0.05,80,50)"
Filter notch_60Hz Design -settle 2.5   2048 "notch(60,100)*notch(300,100)"
#
#--> Filter for the Raw frames
Filter CalNotch Design -settle 2.0 16384 "notch(54.7,30.0)*notch(396.7,200)*notch(1151.5,600.0)"
#
#--->  Power line monitors
#
Glitch L0:PEM-EX_V1      L0:PEM-EX_V1       -sigma 5.0 -snip 0.05 \
 					    -filter notch_60Hz
Glitch L0:PEM-EY_V1      L0:PEM-EY_V1       -sigma 5.0 -snip 0.05 \
 					    -filter notch_60Hz
#
#---> Look for glitches in the calibration lines
Glitch L1:LSC-DARM_CTRL_EXC_DAQ L1:LSC-DARM_CTRL_EXC_DAQ -filter CalNotch \
				-while L1:Both_arms_locked_strict_cm \
 				-sigma 5 -size 0.0001 -snip 0.1 \
 				-segment L1:DMT-CALIB_LINE_GLITCH:1 \
 				-comment "L1 calibration line glitches"
#
#---> Look for glitches in the pulsar injections
#
Glitch L1:SUS-ETMY_SENSOR_SIDE  L1:SUS-ETMY_SENSOR_SIDE \
				-while L1:Both_arms_locked_strict_cm \
                                 -sigma 5 -size 0.0001 -snip 0.1 \
                                 -segment L1:DMT-ETMY_SENSOR_SIDE:1 \
                                 -comment "L1 etmy side sensor glitches"

#---> Look for glitches in the TCS Channels
Glitch L1:TCS-ITMX_PD_ISS_OUT_ACL L1:TCS-ITMX_PD_ISS_OUT_AC \
            -sigma 15 -snip 0.01 -while L1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment L1:DMT-TCS_ITMX_OUT_LOW    \
            -comment "L1 TCS ITMX glitches 15 sigma"
Glitch L1:TCS-ITMX_PD_ISS_OUT_ACM L1:TCS-ITMX_PD_ISS_OUT_AC \
            -sigma 30 -snip 0.01 -while L1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment L1:DMT-TCS_ITMX_OUT_MED    \
            -comment "L1 TCS ITMX glitches 30 sigma"
Glitch L1:TCS-ITMX_PD_ISS_OUT_ACH L1:TCS-ITMX_PD_ISS_OUT_AC \
            -sigma 45 -snip 0.01 -while L1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment L1:DMT-TCS_ITMX_OUT_HIGH    \
            -comment "L1 TCS ITMX glitches 45 sigma"
#
Glitch L1:TCS-ITMY_PD_ISS_OUT_ACL L1:TCS-ITMY_PD_ISS_OUT_AC \
            -sigma 15 -snip 0.01 -while L1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment L1:DMT-TCS_ITMY_OUT_LOW    \
            -comment "L1 TCS ITMY glitches - 15 sigma"
Glitch L1:TCS-ITMY_PD_ISS_OUT_ACM L1:TCS-ITMY_PD_ISS_OUT_AC \
            -sigma 30 -snip 0.01 -while L1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment L1:DMT-TCS_ITMY_OUT_MED    \
            -comment "L1 TCS ITMY glitches 30 sigma"
Glitch L1:TCS-ITMY_PD_ISS_OUT_ACH L1:TCS-ITMY_PD_ISS_OUT_AC \
            -sigma 45 -snip 0.01 -while L1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment L1:DMT-TCS_ITMY_OUT_HIGH    \
            -comment "L1 TCS ITMY glitches 45 sigma"

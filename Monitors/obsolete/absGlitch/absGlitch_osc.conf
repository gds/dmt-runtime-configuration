#
# OSC configuration for LockLoss monitor
#
# $Id$
#

# LockLoss used to look at ASC-QPDX_DC and ASC-QPDY_DC to determine
# the lock state.  These channels are "unreliable": they may give
# inaccurate data that does not reflect or affect lock state.
# We now use LSC-LA_PTRR_NORM (X arm) and LSC-LA_PTRT_NORM (Y arm).
# These two channels are normalized s.t. their values lie in the range
# [0,1] in recombination mode, and [0,thousands] in recycled mode.
# We'll set our threshold at 0.3.

#######################
#                    #
#  Begin H1 Section #
#                  #
###################

#H1:X_arm_locked        meanabove     "H1:LSC-LA_PTRR_NORM"     threshold=0.3
#H1:Y_arm_locked        meanabove     "H1:LSC-LA_PTRT_NORM"     threshold=0.3
#
#H1:Both_arms_locked    boolean       "H1:X_arm_locked & H1:Y_arm_locked"
#
#H1:X_arm_lock_acquired transitup   "H1:X_arm_locked"
#H1:X_arm_lock_lost     transitdown "H1:X_arm_locked"
#
#H1:Y_arm_lock_acquired transitup   "H1:Y_arm_locked"
#H1:Y_arm_lock_lost     transitdown "H1:Y_arm_locked"
#
#H1:Both_arms_lock_acquired transitup   "H1:Both_arms_locked"
#H1:Both_arms_lock_lost     transitdown "H1:Both_arms_locked"

#
# Locked state of Mode Cleaner (From R. Rahkola <rrahkola@darkwing.uoregon.edu>)
#
#H1:Mode_Cleaner_Powerup        meanbelow    "H1:IOO-EO_SHTR_PD_POWER_MON" threshold=.0015
#H1:Mode_Cleaner_ShutterState   valueabove   "H1:IOO-EO_SHTR_STATE" threshold=.9
#H1:Mode_Cleaner_locked         boolean      "H1:Mode_Cleaner_Powerup & H1:Mode_Cleaner_ShutterState"
#H1:Mode_Cleaner_lock_acquired  transitup    "H1:Mode_Cleaner_locked"
#H1:Mode_Cleaner_lock_lost      transitdown  "H1:Mode_Cleaner_locked"

###################
#                  #
#  End H1 Section   #
#                    #
#######################


#######################
#                    #
#  Begin H2 Section #
#                  #
###################

H2:X_arm_locked        meanabove     "H2:LSC-LA_PTRR_NORM"     threshold=0.3
H2:Y_arm_locked        meanabove     "H2:LSC-LA_PTRT_NORM"     threshold=0.3

H2:Both_arms_locked    boolean       "H2:X_arm_locked & H2:Y_arm_locked"

#H2:X_arm_lock_acquired transitup   "H2:X_arm_locked"
#H2:X_arm_lock_lost     transitdown "H2:X_arm_locked"

#H2:Y_arm_lock_acquired transitup   "H2:Y_arm_locked"
#H2:Y_arm_lock_lost     transitdown "H2:Y_arm_locked"

#H2:Both_arms_lock_acquired transitup   "H2:Both_arms_locked"
#H2:Both_arms_lock_lost     transitdown "H2:Both_arms_locked"

#
# Locked state of Mode Cleaner (From R. Rahkola <rrahkola@darkwing.uoregon.edu>)
#
#H2:Mode_Cleaner_Powerup        meanbelow    "H2:IOO-EO_SHTR_PD_POWER_MON" threshold=.0015
#H2:Mode_Cleaner_ShutterState   valueabove   "H2:IOO-EO_SHTR_STATE" threshold=.9
#H2:Mode_Cleaner_locked         boolean      "H2:Mode_Cleaner_Powerup & H2:Mode_Cleaner_ShutterState"
#H2:Mode_Cleaner_lock_acquired  transitup    "H2:Mode_Cleaner_locked"
#H2:Mode_Cleaner_lock_lost      transitdown  "H2:Mode_Cleaner_locked"

###################
#                  #
#  End H2 Section   #
#                    #
#######################


#######################
#                    #
#  Begin L1 Section #
#                  #
###################

#L1:X_arm_locked        meanabove     "L1:LSC-LA_PTRR_NORM"     threshold=0.3
#L1:Y_arm_locked        meanabove     "L1:LSC-LA_PTRT_NORM"     threshold=0.3
#
#L1:Both_arms_locked    boolean       "L1:X_arm_locked & L1:Y_arm_locked"
#
#L1:X_arm_lock_acquired transitup   "L1:X_arm_locked"
#L1:X_arm_lock_lost     transitdown "L1:X_arm_locked"
#
#L1:Y_arm_lock_acquired transitup   "L1:Y_arm_locked"
#L1:Y_arm_lock_lost     transitdown "L1:Y_arm_locked"
#
#L1:Both_arms_lock_acquired transitup   "L1:Both_arms_locked"
#L1:Both_arms_lock_lost     transitdown "L1:Both_arms_locked"
#
##
## Locked state of Mode Cleaner (From R. Rahkola <rrahkola@darkwing.uoregon.edu>)
##
##  Note threshold reset by jgz on 11/15/01
#L1:Mode_Cleaner_Powerup        meanbelow    "L1:IOO-EO_SHTR_PD_POWER_MON" threshold=.15
#L1:Mode_Cleaner_ShutterState   valueabove   "L1:IOO-EO_SHTR_STATE" threshold=.9
#L1:Mode_Cleaner_locked         boolean      "L1:Mode_Cleaner_Powerup & L1:Mode_Cleaner_ShutterState"
#L1:Mode_Cleaner_lock_acquired  transitup    "L1:Mode_Cleaner_locked"
#L1:Mode_Cleaner_lock_lost      transitdown  "L1:Mode_Cleaner_locked"

###################
#                  #
#  End L1 Section   #
#                    #
#######################

#  Parameters from /archive/home/xsiemens/hoft/S5RecoveryApril2007/L1/DONEPhase6/S5strainDAGL1_825465258-999999999.ini
#
  -stride 16
  -asq-channel     "L1:LSC-AS_Q"
  -darm-channel    "L1:LSC-DARM_CTRL"
  -darmerr-channel "L1:LSC-DARM_ERR"
  -exc-channel     "L1:LSC-DARM_CTRL_EXC_DAQ"
  -cal-line-freq 396.7
  -olg-re      -0.45099560283524
  -olg-im      -0.10300051947141
  -servo-re    -1.31634870581544
  -servo-im     1.18312455135791
  -whitener-re  0.00987134835155 
  -whitener-im -0.00160677181035
  -filters-file S5L1Filters_825465258-999999999.txt
  -wings   16

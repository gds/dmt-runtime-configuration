* * * * * /usr/bin/SPI.pl --gds-config=$HOME/dmtspi --cache=/gds-h1/dmt/dmtspi/cache > /var/tmp/spi.out
*/10 * * * * /usr/bin/USAGE.tcsh --gds-config=$HOME/dmtspi > /var/tmp/usage.out
* * * * * $HOME/status/status-monitor-h1dmt1 > /var/tmp/status.out
3 8 * * 2 $HOME/scripts/condense_segments_LHO > /var/tmp/condense.out
*/5 * * * * $HOME/status/all_files-lho > /var/tmp/all-files.out
12 1 * * * $HOME/scripts/sensemon_rename > /var/tmp/sensemon.out
10 * * * * (/bin/rsync --verbose -W --archive --compress --timeout=3600 --exclude conlog/  --exclude current --exclude frames/ /gds-h1/dmt/trends/ ldasadm@ldas-cit.ligo.caltech.edu:/archive/frames/dmt/H1/trends > /var/tmp/rsync.trends.out 2>&1)

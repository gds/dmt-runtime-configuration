* * * * * /usr/bin/SPI.pl --gds-config=$HOME/dmtspi --cache=/gds/dmt/dmtspi/cache > /var/tmp/spi.out
*/10 * * * * /usr/bin/USAGE.tcsh --gds-config=$HOME/dmtspi > /var/tmp/usage.out
* * * * * $HOME/status/status-monitor-l1dmt1 > /var/tmp/status.out
*/5 * * * * $HOME/status/all_files-llo > /var/tmp/all-files.out
10 * * * * (/bin/rsync --verbose -W --archive --compress --timeout=3600 --exclude conlog/  --exclude current --exclude frames/ /gds/dmt/trends/ ldasadm@ldas-cit.ligo.caltech.edu:/archive/frames/dmt/L1/trends > /var/tmp/rsync.trends.out 2>&1)
10 2 * * * $HOME/scripts/sensemon_rename > /var/tmp/sensemon.out
